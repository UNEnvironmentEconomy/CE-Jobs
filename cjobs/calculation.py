#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np


def inv(x):
    """
    Returns inverse by dividing by 1 and eliminating inf and nan values
    """
    mask = (x == 0)
    x[~mask] = 1/x[~mask]
    return x


def IOT_expansion(emp_data, midx, Z, Y, m=False):
    # Generate frame of size (mxn)
    # filled with subsectors share of parent sector
    exp = pd.concat(
        [emp_data['ppn_sect']]*len(Z),
        axis=1, keys=Z.columns)
    exp.index = emp_data['nlevel1']

    # For every column, if index label does not match
    # column label set cell to zero
    # (Still ok because of small size of matrix,
    # but boolean mask needed if exports to be included)
    exp_left = exp.apply(
        lambda x: [x[e] if i in x.name else 0 for e, i in enumerate(x.index)],
        axis=0)

    Z = exp_left @ Z @ exp_left.T
    Z.index, Z.columns = midx, midx
    Z = Z.fillna(0)

    Y = exp_left @ Y
    Y.index = midx
    Y = Y.fillna(0)

    output_m = list()
    if not isinstance(m, bool):
        if not isinstance(m, list):
            m = [m]
        deleteme = pd.DataFrame(index=midx.get_level_values('nlevel1'))
        for n in m:
            n.index.name = 'nlevel1'
            n = deleteme.join(n)
            output_m.append(n)

    return(Z, Y, output_m)


def _calculate_multipiers(row, ax, core):
    sector_type = row.name[-1]
    if row.sum() > 0:
        try:
            multiplyer = row.xs("core", level=3).sum() / row.sum()
        except:  # No core sectors on this city (!) # TODO: review, no core sectors!
            multiplyer = 0
    else:
        multiplyer = 0
    if sector_type == "core" and core:
        return 1
    elif sector_type == "enabling" and ax == 1:
        return multiplyer
    elif sector_type == "indirectly" and ax == 0:
        return multiplyer
    return 0


def CE_multipliers(Z, midx):
    # Calculate circular output multipliers (circular market shares)
    i_inpmul = Z.apply(
            lambda x: _calculate_multipiers(x, 1, True), axis=1)
    i_outmul = Z.apply(
            lambda x: _calculate_multipiers(x, 0, True), axis=0)

    # Calculate coefficient matrix from arrays
    outmul_frame = pd.concat([i_outmul]*len(i_outmul), axis=1, keys=midx)
    inpmul_frame = pd.concat([i_inpmul]*len(i_inpmul), axis=1, keys=midx)

    # Delete enabling and Indirect multipliers
    outmul_frame.loc[
        (slice(None), slice(None), slice(None), ['enabling', 'indirectly']),
        (slice(None), slice(None), slice(None), 'indirectly')
        ] = 0
    inpmul_frame.loc[
        (slice(None), slice(None), slice(None), 'enabling'),
        (slice(None), slice(None), slice(None), ['enabling', 'indirectly'])
        ] = 0

    mulmat = outmul_frame * inpmul_frame.T

    multiplyer_inp = Z.apply(
            lambda x: _calculate_multipiers(x, 0, False), axis=0)
    inpmul_frame = pd.concat(
            [multiplyer_inp]*len(multiplyer_inp), axis=1, keys=midx)

    multiplyer_out = Z.apply(
            lambda x: _calculate_multipiers(x, 1, False), axis=1)
    outmul_frame = pd.concat(
            [multiplyer_out]*len(multiplyer_out), axis=1, keys=midx)
    outmul_frame.loc[
        (slice(None), slice(None), slice(None), 'enabling'),
        (slice(None), slice(None), slice(None), 'indirectly')
        ] = 0
    outmul_frame.loc[
        (slice(None), slice(None), slice(None), 'enabling'),
        (slice(None), slice(None), slice(None), 'enabling')
        ] = 1

    return outmul_frame, inpmul_frame, mulmat


def IOA(emp_data, midx, Z, Y, mulmat, inpmul, outmul, m=False, g=False):
    """
    Calculate counterfactual technical coefficient matrix
    """
    # Calculate technical coefficient matrix from intermediate demand matrix
    x_sum = Z.sum(axis=1) + Y.Y  # Total output (expanded)
    A = pd.DataFrame(Z @ inv(np.diag(x_sum)))  # Technical coefficients
    A.columns = Z.columns

    # Apply Leontief model and
    # caulcualte counterfactual total requirement matrix
    L = pd.DataFrame(np.linalg.inv((np.eye(len(A)) - A)), index=midx, columns=midx)
    L_cntf = L.mul(mulmat)
    L_cntf_in = L.mul(inpmul)
    L_cntf_out = L.mul(outmul)

    # Calculate employent intensity vector
    emp_city = emp_data['city_job']
    b = pd.Series(
            emp_city @ inv(np.diag(x_sum)),
            index=midx, name='Employment intensity')

    # Total baseline and counterfactual employment
    r = np.diag(b.T @ L) @ Y
    r.index = midx
    r.columns = ['nr_jobs']

    r_cntf = np.diag(b.T @ L_cntf) @ Y
    r_cntf_in = np.diag(b.T @ L_cntf_in) @ Y
    r_cntf_out = np.diag(b.T @ L_cntf_out) @ Y

    #TODO: Validate r_mat computation
    if not isinstance(m, bool):
        r_mat = pd.DataFrame(m)
        r_mat.index = midx
        r_mat.columns = ['mat_cof']

    #TODO: Validate r_ghg computation
    if not isinstance(g, bool):
        r_ghg = pd.DataFrame(g)
        r_ghg.index = midx
        r_ghg.columns = ['ghg_cof']

    r_cntf.index = midx
    r_cntf.columns = ['nr_core_jobs']

    r_cntf_in.index = midx
    r_cntf_in.columns = ['nr_enabling_jobs']

    r_cntf_out.index = midx
    r_cntf_out.columns = ['nr_indirectly_jobs']

    r_circular = pd.DataFrame(
            r_cntf.iloc[:, 0]
            .add(r_cntf_in.iloc[:, 0])
            .add(r_cntf_out.iloc[:, 0])
            .divide(r.iloc[:, 0], fill_value=0),
            index=midx,
            columns=['circularity']
            ).fillna(0)
    r_circular.loc[(slice(None), slice(None), slice(None), 'core'), :] = 1

    if not isinstance(m, bool):
        if not isinstance(g, bool):
            r_circular_multiplyer = (r_circular.iloc[:, 0] + r_mat.iloc[:, 0] + r_ghg.iloc[:, 0]) / 3
        else:
            r_circular_multiplyer = (r_circular.iloc[:, 0] + r_mat.iloc[:, 0]) / 2
    elif not isinstance(g, bool):
        r_circular_multiplyer = (r_circular.iloc[:, 0] + r_ghg.iloc[:, 0]) / 2

    total_circular_jobs = r.iloc[:, 0].mul(r_circular_multiplyer)

    r_total = pd.DataFrame(total_circular_jobs, columns=['total_circular_jobs'])

    if not isinstance(m, bool):
        r_mat_jobs = pd.DataFrame(
            r.iloc[:, 0].mul(r_mat.iloc[:, 0]))
        r_mat_jobs.index = midx
        r_mat_jobs.columns = ['mid_circular_jobs']

    if not isinstance(g, bool):
        r_ghg_jobs = pd.DataFrame(
            r.iloc[:, 0].mul(r_ghg.iloc[:, 0]))
        r_ghg_jobs.index = midx
        r_ghg_jobs.columns = ['ghg_circular_jobs']

    if not isinstance(m, bool):
        if not isinstance(g, bool):
            outtable = pd.concat(
                [
                    r, r_cntf,
                    r_cntf_in, r_cntf_out,
                    r_mat, r_mat_jobs,
                    r_ghg, r_ghg_jobs,
                    r_circular, r_total
                ],
                axis=1)
        else:
            outtable = pd.concat(
                    [
                        r, r_cntf,
                        r_cntf_in, r_cntf_out,
                        r_mat, r_mat_jobs,
                        r_circular, r_total
                    ],
                    axis=1)
    elif not isinstance(g, bool):
        outtable = pd.concat(
            [
                r, r_cntf,
                r_cntf_in, r_cntf_out,
                r_ghg, r_ghg_jobs,
                r_circular, r_total
            ],
            axis=1)
    else:
        outtable = pd.concat(
                [
                    r, r_cntf,
                    r_cntf_in, r_cntf_out,
                    r_circular, r_total
                ],
                axis=1)

    return outtable


def main():
    # TODO: fix_me
    # =========================================================================
    # INITIALIZATION - Load WIOD, scaling data and extract region of interest
    # =========================================================================

    # Load Data
    from sample_data import test_data
    Z, Y, emp_data, midx, m = test_data()

    # =========================================================================
    # STEP 1 - Intermediate demand, final demand and exports expansion
    # =========================================================================

    # Expand selected country
    Z, Y = IOT_expansion(emp_data, midx, Z, Y)

    # =========================================================================
    # STEP 2 - Calculate circular output and input multipliers
    # =========================================================================

    # Generate IO multipliers matrix
    _, _, mulmat = CE_multipliers(Z, midx)

    # =========================================================================
    # STEP 3 - Create counterfactual technical coefficient matrix A
    # and apply Leontief model
    # =========================================================================

    outtable = IOA(emp_data, midx, Z, Y, mulmat)
    outtable.to_excel('output/result.xlsx')


if __name__ == "__main__":
    main()
