#!/usr/bin/env python
# coding: utf-8

import os
import json
from uuid import uuid4
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pymrio
from IPython.core.display import display, HTML

from smum.urbanmet.tdModel import IOTables
from smum.urbanmet.utils import get_factor

from .calculation import IOA, IOT_expansion, CE_multipliers


class IOT:
    def __init__(self, connection_str, config_file, verbose=False, model='exiobase', year=None, saveall=False):

        if os.path.exists(config_file):
            with open(config_file, 'r') as JSON:  # Fail Fast!
                self.rules_dic = json.load(JSON)
        else:
            if isinstance(config_file, str):
                self.rules_dic = json.loads(config_file)
            elif isinstance(config_file, dict):
                self.rules_dic = config_file
            else:
                raise TypeError('Unknown type {} for config_file'.format(type(config_file)))

        if os.path.exists(connection_str):
            file_path = connection_str
        elif os.path.exists(connection_str.split('_data')[0]):
            file_path = connection_str
        else:
            raise FileNotFoundError("Can't find file: {}".format(connection_str))

        if not year and model.lower() == 'eora' and file_path:
            raise Exception("""
            When using a physical file for EORA you need to specify the EORA database year.
            See: https://pymrio.readthedocs.io/en/latest/api_doc/pymrio.parse_eora26.html """)

        if "GENERAL" in self.rules_dic.keys():
            general = self.rules_dic.pop('GENERAL')
            for country in self.rules_dic.keys():
                self.rules_dic[country].update(general)
                if isinstance(self.rules_dic[country]['pop_share'], str):
                    self.rules_dic[country]['pop_share'] = eval(
                        self.rules_dic[country]['pop_share'])

        # exio3                             EORA
        # -----------------                -------------
        # .satellite.D_pba  (RMI)           .Q.D_pba
        # .satellite.D_imp  (/$)            .Q.D_imp
        # .satellite.D_exp  (/$)            .Q.D_exp
        # .satellite.D_cba  (employment)    .Q.D_cba
        # .Z                (IO)            .Z
        # .Y                (Total output)  .Y

        print('parse data', end='\t')
        # ROOT_PATH = './data/exiobase/IOT_2016_ixi_data/'
        if str(model).lower() == 'exiobase':
            self.model = 'exiobase'
            self.mrio = pymrio.parse_exiobase3(path=file_path)
        # ROOT_PATH = './data/eora/'
        elif str(model).lower() == 'eora':  # EORA
            self.model = 'eora'
            self.mrio = pymrio.parse_eora26(year=year, path=file_path)
        else:
            self.model = None
            print('Unknown model: {}'.format(model))
            raise Exception('Unknown model')
        print('OK')
        print('cal all', end='\t')
        self.mrio.calc_all()
        print('OK')
        if saveall:
            print('save all', end='\t')
            self.mrio.save_all(file_path)
            print('OK')
        print("countries: {}".format(" - ".join(self.rules_dic.keys())))
        self.verbose = verbose

    def plot_mid(
            self,
            query="(BE > 0) or (BR > 0)",
            save_path=False,
            **kwargs):
        """
        Plot Material Import Dependency (MID)
        """
        if 'figsize' not in kwargs:
            kwargs['figsize'] = (15, 5)
        clone_IMP = pd.DataFrame(index=self.IMP.xs(self.countries[0]).index)
        for c in self.countries:
            clone_IMP.loc[:, c] = self.IMP.loc[c, 'imp']
        clone_IMP.query(query, inplace=True)

        f, ax = plt.subplots(1, 1, **kwargs)
        f.suptitle('Material Import Dependency (MID)', fontsize=16)

        clone_IMP.dropna().plot.bar(ax=ax)
        if save_path:
            save_ext = save_path.split('.')[1]
            f.savefig(
                save_path, format=save_ext,
                dpi=300, bbox_inches='tight',
                transparent=True)

    def print_pie(
            self,
            country,
            title=None,
            save_path=False,
            nmax=0):
        """
        Plot CE shares
        """
        if save_path:
            print(save_path)
            html_file = open(save_path, 'w')
        pd.options.display.float_format = '{:,.2f}'.format
        outtable = self.country_data[country]['outtable'].fillna(0)
        fig, ax = plt.subplots(figsize=(10, 10), subplot_kw=dict(aspect="equal"))

        lables = outtable.index.get_level_values(2)

        top_val = outtable.sort_values(
                'total_circular_jobs',
                ascending=False)
        top_val = top_val.reset_index().loc[
                :, ['description_english',
                    'nr_jobs',
                    'total_circular_jobs']]
        top_val = top_val.astype(
                {'nr_jobs': int, 'total_circular_jobs': int})
        top_val_table = top_val.copy()
        top_val_table['share'] = round(top_val['total_circular_jobs']
                                       .div(top_val['total_circular_jobs']
                                       .sum()), 4)
        html_tab = "<h1>Top Economy Sectors for Implementation</h1>"
        html_tab += "<h2>Top 10 Circular Economy Jobs economic sectors</h2>"
        top_val_table.loc[:, 'share'] = pd.Series(
            ["{0:.2f}%".format(val * 100) for val in top_val_table['share']], index=top_val_table.index)
        top_val_table.loc[:, 'nr_jobs'] = pd.Series(
            [int(val) for val in top_val_table['nr_jobs']], index=top_val_table.index)
        top_val_table.loc[:, 'ce_jobs'] = pd.Series(
            [int(val) for val in top_val_table['total_circular_jobs']], index=top_val_table.index)
        html_tab += top_val_table.loc[:, ['description_english', 'nr_jobs', 'ce_jobs', 'share']].head(10).to_html()

        if 'mid_circular_jobs' in outtable.columns:
            top_val_mat = outtable.sort_values(
                    'mid_circular_jobs',
                    ascending=False)
            top_val_mat = top_val_mat.reset_index().loc[
                    :, ['description_english',
                        'nr_jobs',
                        'mid_circular_jobs']]
            top_val_mat = top_val_mat.astype(
                    {'nr_jobs': int, 'mid_circular_jobs': int})
            top_val_table = top_val_mat.copy()
            top_val_table['share'] = round(top_val_table['mid_circular_jobs']
                                           .div(top_val_table['mid_circular_jobs']
                                           .sum()), 4)
            html_tab += "<h2>Top 10 Circular Economy Jobs economic sectors (only MID)</h2>"
            top_val_table.loc[:, 'share'] = pd.Series(
                ["{0:.2f}%".format(val * 100) for val in top_val_table['share']], index=top_val_table.index)
            top_val_table.loc[:, 'nr_jobs'] = pd.Series(
                [int(val) for val in top_val_table['nr_jobs']], index=top_val_table.index)
            top_val_table.loc[:, 'm_jobs'] = pd.Series(
                [int(val) for val in top_val_table['mid_circular_jobs']], index=top_val_table.index)
            html_tab += top_val_table.loc[:, ['description_english', 'nr_jobs', 'm_jobs', 'share']].head(10).to_html()
        display(HTML(html_tab))  # to html
        if save_path:
            html_file.write(html_tab)

        if not nmax:
            nmax = top_val.iloc[9, -1]

        html_tite = "<h1>Distribution of economic sectors</h1>"
        display(HTML(html_tite))
        if save_path:
            html_file.write(html_tite)

        outtable.loc[:, 'total_circular_jobs'] = outtable.loc[:, 'total_circular_jobs'].fillna(0)
        outtable.loc[outtable.total_circular_jobs < 0] = 0.00
        wedges, texts = ax.pie(
                outtable.loc[:, 'total_circular_jobs'],
                wedgeprops=dict(width=0.5), startangle=-40)

        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        kw = dict(arrowprops=dict(arrowstyle="-"),
                  bbox=bbox_props, zorder=0, va="center")

        for i, p in enumerate(wedges):
            if outtable.iloc[i, -1] >= nmax:
                ang = (p.theta2 - p.theta1)/2. + p.theta1
                y = np.sin(np.deg2rad(ang))
                x = np.cos(np.deg2rad(ang))
                horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
                connectionstyle = "angle,angleA=0,angleB={}".format(ang)
                kw["arrowprops"].update({"connectionstyle": connectionstyle})
                lable = lables[i]
                if len(lable) >= 30:
                    lable = lable[:30]
                ax.annotate(lable, xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                            horizontalalignment=horizontalalignment, **kw)

        ax.set_title(title, fontsize=16)
        html_tab = self.print_table(country)

        if save_path:
            print(save_path)
            save_name = "{}/images/{}.svg".format(
                '/'.join(save_path.split('.')[0].split('/')[:-1]), uuid4())
            print(save_name)
            fig.savefig(
                save_name, format='svg',
                dpi=300, bbox_inches='tight',
                transparent=True)
            html_image = """< object
            type = "image/svg+xml"
            data = "{}" > < / object >""".format(save_name)
            html_file.write(html_image)

            html_file.write(html_tab)
            html_file.close()

    def print_table(self, country, groupby='ntypequalifier'):
        pd.options.display.float_format = '{:,.2f}'.format
        outtable = self.country_data[country]['outtable'].fillna(0)
        _sum = outtable.loc[
                :, ['nr_jobs', 'total_circular_jobs']].groupby(groupby).sum()
        use_mat = False
        if 'mid_circular_jobs' in outtable.columns:
            _sum_mat = outtable.loc[
                    :, ['nr_jobs', 'mid_circular_jobs']].groupby(groupby).sum()
            use_mat = True

        html_tab = "<h1>Summary:</h1>"
        html_tab += "<h2>Summary by circularity type</h2>"

        html_tab += _sum.join(
                        _sum.div(_sum.sum()).apply(
                            lambda x: ["{:0.1%}".format(i) for i in x]),
                            rsuffix='_s').to_html()

        html_tab += """
         <div style="position: relative; text-align: center;">
            <img src="data/circle.png" alt="circle" style="width:30%;">
            <div style="position: absolute; top: 45%; left: 12%;">{:0.2%}</div>
        </div> 
        """.format(
            _sum.sum().loc['total_circular_jobs'] /
            _sum.sum().loc['nr_jobs'])

        if use_mat:

            html_tab += "<h2>Summary by circularity type (only MID)</h2>"

            html_tab += _sum_mat.join(
                    _sum_mat.div(_sum_mat.sum()).apply(
                        lambda x: ["{:0.1%}".format(i) for i in x]),
                    rsuffix='_s').to_html()

            html_tab += """
             <div style="position: relative; text-align: center;">
                <img src="data/circle.png" alt="circle" style="width:30%;">
                <div style="position: absolute; top: 45%; left: 12%;">{:0.2%}</div>
            </div> 
            """.format(
                _sum_mat.sum().loc['mid_circular_jobs'] /
                _sum_mat.sum().loc['nr_jobs'])

        display(HTML(html_tab))
        return html_tab

    def _select_countries(self, countries):
        if not isinstance(countries, list):
            self.countries = [countries]
        else:
            self.countries = countries
        self.country_data = dict()
        for country in self.countries:
            self.country_data[country] = dict()
        self._get_MID()
        self.get_employment()
        self._get_GHG()

    def _compute_global_imp(self):
        """
        Compute Global MID based on Raw Material Equivalent
        """
        print("Compute Global MID", end='\t')

        # Domestic Extraction (D_pba)
        if self.model == 'exiobase':
            D_pba = self.mrio.satellite.D_pba.groupby(level=[1], axis=1).sum()
        elif self.model == 'eora':
            D_pba = self.mrio.Q.D_pba.groupby(level=[1], axis=1).sum()
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')

        inx = ['Domestic Extraction' in i for i in D_pba.index]
        D_pba = D_pba.loc[inx]

        # Raw Material Equivalent Imports (D_imp)
        if self.model == 'exiobase':
            D_imp = self.mrio.satellite.D_imp.groupby(level=[1], axis=1).sum()
        elif self.model == 'eora':
            D_imp = self.mrio.Q.D_imp.groupby(level=[1], axis=1).sum()
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        inx = ['Domestic Extraction' in i for i in D_imp.index]
        D_imp = D_imp.loc[inx]

        # Raw Material Equivalent Exports (D_exp)
        if self.model == 'exiobase':
            D_exp = self.mrio.satellite.D_exp.groupby(level=[1], axis=1).sum()
        elif self.model == 'eora':
            D_exp = self.mrio.Q.D_exp.groupby(level=[1], axis=1).sum()
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        inx = ['Domestic Extraction' in i for i in D_exp.index]
        D_exp = D_exp.loc[inx]

        # Material Import Dependency (MID)
        self.global_IMP = D_imp.sum().div(D_pba.sum() + D_imp.sum())# + D_exp.sum())
        print(self.global_IMP.sum())
        self.global_IMP.name = 'global'

    def _get_GHG(self):
        """
        Compute GHG
        """

        print("Compute GHG: ", end='\t')
        # Emissions (D_pba)
        if self.model == 'exiobase':
            D_pba = self.mrio.satellite.D_pba.loc[:, self.countries]
            inx = ['GHG' in i for i in D_pba.index]
        elif self.model == 'eora':
            D_pba = self.mrio.Q.D_pba.loc[:, self.countries]
            inx = ['PRIMAP' in i[1] for i in D_pba.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_pba = D_pba.loc[inx]

        # GHG Imports (D_imp)
        if self.model == 'exiobase':
            D_imp = self.mrio.satellite.D_imp.loc[:, self.countries]
            inx = ['GHG' in i for i in D_imp.index]
        elif self.model == 'eora':
            D_imp = self.mrio.Q.D_imp.loc[:, self.countries]
            inx = ['PRIMAP' in i[1] for i in D_imp.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_imp = D_imp.loc[inx]

        # GHG Exports (D_exp)
        if self.model == 'exiobase':
            D_exp = self.mrio.satellite.D_exp.loc[:, self.countries]
            inx = ['GHG' in i for i in D_exp.index]
        elif self.model == 'eora':
            D_exp = self.mrio.Q.D_exp.loc[:, self.countries]
            inx = ['PRIMAP' in i[1] for i in D_exp.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_exp = D_exp.loc[inx]

        # Total GHG (Imports + Local consumption + Exports)
        self.GHG = D_pba.sum() + D_imp.sum() + D_exp.sum()
        # TODO: validate
        print(self.GHG.sum())
        self.GHG = self.GHG.fillna(0)

    def _get_MID(self):
        """
        Compute MID based on Raw Material Equivalent
        """

        self._compute_global_imp()

        print("Compute MID: ", end='\t')
        # Domestic Extraction (D_pba)
        if self.model == 'exiobase':
            D_pba = self.mrio.satellite.D_pba.loc[:, self.countries]
            inx = ['Domestic Extraction' in i for i in D_pba.index]
        elif self.model == 'eora':
            D_pba = self.mrio.Q.D_pba.loc[:, self.countries]
            inx = ['Raw material inputs' in i[1] for i in D_pba.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_pba = D_pba.loc[inx]

        # Raw Material Equivalent Imports (D_imp)
        if self.model == 'exiobase':
            D_imp = self.mrio.satellite.D_imp.loc[:, self.countries]
            inx = ['Domestic Extraction' in i for i in D_imp.index]
        elif self.model == 'eora':
            D_imp = self.mrio.Q.D_imp.loc[:, self.countries]
            inx = ['Raw material inputs' in i[1] for i in D_imp.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_imp = D_imp.loc[inx]

        # Raw Material Equivalent Exports (D_exp)
        if self.model == 'exiobase':
            D_exp = self.mrio.satellite.D_exp.loc[:, self.countries]
            inx = ['Domestic Extraction' in i for i in D_exp.index]
        elif self.model == 'eora':
            D_exp = self.mrio.Q.D_exp.loc[:, self.countries]
            inx = ['Raw material inputs' in i[1] for i in D_exp.index]
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')
        D_exp = D_exp.loc[inx]

        # Material Import Dependency (MID)
        self.IMP = D_imp.sum().div(D_pba.sum() + D_imp.sum())# + D_exp.sum())
        #self.IMP = self.IMP.replace(float('inf'), 0.5)  # TODO: validate
        print(self.IMP.sum())
        self.IMP = self.IMP.fillna(0)

    def get_employment(self, employment_file=False):
        """
        Compute employment vectors
        """
        if isinstance(employment_file, bool):
            print("Compute Employment", end='\t')
        elif isinstance(employment_file, dict) and all(
                employment_file.keys() in self.countries):
            print("Compute Employment using external file")
        else:
            raise Exception(
                    """Unknow input type for emplyment_data:
                    Expected: {}; Got: {}""".format(
                        type(dict), type(employment_file)))

        if self.model == 'exiobase':
            self.employment = self.mrio.satellite.D_pba.loc[
                ['Employment:' in i and 'Vulnerable' not in i
                 for i in self.mrio.satellite.D_cba.index],
                self.countries]
            array = np.asanyarray(
                [i.split('Employment: ')[-1].split() for i in self.employment.index])
            new_index = pd.MultiIndex.from_arrays(
                array.T, names=('skil', 'gender'))
            self.employment.index = new_index
            print('OK')
        elif self.model == 'eora':
            self.employment = None
            print('FAIL')
        else:
            print('Unknown model: {}'.format(self.model))
            raise Exception('Unknown model')

    def _downscale_tab(self, name, d_file, target, **kwargs):
        io = self.mrio.Z.loc[name, name]
        inx = [i in self.agg_data.loc[:, 'SECTOR CODE'] for i in io.index]
        io = io.loc[inx, inx]

        sum_in = io.sum(axis=0).sum()
        sum_ou = io.sum(axis=1).sum()

        new_sectors = [self.agg_data.loc[i, 'SECTOR CODE'] for i in io.index]
        for _ in range(2):
            io.loc[:, 'Sector'] = new_sectors
            io = io.groupby('Sector').sum().T

        # Validate aggregation
        np.testing.assert_almost_equal(sum_in, io.sum(axis=0).sum(), decimal=3)
        np.testing.assert_almost_equal(sum_ou, io.sum(axis=1).sum(), decimal=3)

        # Load Domestic data
        self.IOtab.add(io, name=name)

        # Data for computing factor
        dat = pd.read_csv(d_file, **kwargs)
        dat = dat.groupby('io_code').sum()
        dat = dat.T
        dat.index = [name, target]

        # Compute factor
        d_factor = get_factor(
            self.IOtab, dat,
            source=name, target=target, mod='downscale')

        # Downscale the I-O tables
        self.IOtab.tables[name].update(d_factor)

    def downscale_data(self, data_file):
        """
        Downscale IO Table with IPF

        data_file = 'data/agg.csv'
        self.rules_dic = {
                'BR': {
                    'file': "./data/recife_employmentdata.csv",
                    'groupby': "nlevel1",
                    'on_var': "city_job",
                    'index_cols': ['nlevel1', 'nlevel3',
                        'description_english', 'ntypequalifier'],
                    'name': "Rec",
                    'pop_share': 1208542 / 11515793,
                    'sector_code': "SECTOR CODE",
        Downscale IO Table with IPF

        data_file = 'data/agg.csv'
        self.rules_dic = {
                'BR': {
                    'file': "./data/recife_employmentdata.csv",
                    'groupby': "nlevel1",
                    'on_var': "city_job",
                    'index_cols': ['nlevel1', 'nlevel3',
                        'description_english', 'ntypequalifier'],
                    'name': "Rec",
                    'pop_share': 1208542 / 11515793,
                    'sector_code': "SECTOR CODE",
                    'kwargs_data': {
                        'usecols': [1, 2, 3, 4, 6],
                        'header': 0,
                        'names': [
                            'city_job', 'nlevel3',       'description_english',
                            'ntypequalifier', '   nlevel1']},
                    'kwargs': {'usecols': [0, 1, 6]}
                    },
                'BE': {
                    'file': "./data/brusels_employmentdata.csv",
                    'groupby': "nlevel1",
                    'on_var': "city_job",
                    'index_cols': ['nlevel1', 'nlevel3',
                        'description_english', 'ntypequalifier'],
                    'name': "Bru",
                    'pop_share': 1555039 / 210147125,
                    'sector_code': "SECTOR CODE",
                    'kwargs_data': {
                        'usecols': [1, 2, 3, 4, 6],
                        'header': 0,
                        'names': [
                            'city_job', 'nlevel3', 'description_english',
                            'ntypequalifier', 'nlevel1']},
                    'kwargs': {'usecols': [0, 1, 6]},
                    },
                }

                    'kwargs_data': {
                        'usecols': [1, 2, 3, 4, 6],
                        'header': 0,
                        'names': [
                            'city_job', 'nlevel3',       'description_english',
                            'ntypequalifier', '   nlevel1']},
                    'kwargs': {'usecols': [0, 1, 6]}
                    },
                'BE': {
                    'file': "./data/brusels_employmentdata.csv",
                    'groupby': "nlevel1",
                    'on_var': "city_job",
                    'index_cols': ['nlevel1', 'nlevel3',
                        'description_english', 'ntypequalifier'],
                    'name': "Bru",
                    'pop_share': 1555039 / 210147125,
                    'sector_code': "SECTOR CODE",
                    'kwargs_data': {
                        'usecols': [1, 2, 3, 4, 6],
                        'header': 0,
                        'names': [
                            'city_job', 'nlevel3', 'description_english',
                            'ntypequalifier', 'nlevel1']},
                    'kwargs': {'usecols': [0, 1, 6]},
                    },
                }
        """
        self._select_countries([i for i in self.rules_dic.keys()])
        self.agg_data = pd.read_csv(data_file, index_col=1)
        # Create I-O instance
        self.IOtab = IOTables(verbose=self.verbose)

        for country in self.countries:
            assert(country in self.rules_dic.keys())
            data_file = self.rules_dic[country]['file']
            groupby = self.rules_dic[country]['groupby']
            on_var = self.rules_dic[country]['on_var']
            index_cols = self.rules_dic[country]['index_cols']
            sector_code = self.rules_dic[country]['sector_code']
            if isinstance(self.rules_dic[country]['kwargs_data'], str):
                output_kwargs_data = json.loads(self.rules_dic[country]['kwargs_data'])
            else:
                output_kwargs_data = self.rules_dic[country]['kwargs_data']
            if isinstance(self.rules_dic[country]['kwargs'], str):
                output_kwargs = json.loads(self.rules_dic[country]['kwargs'])
            else:
                output_kwargs = self.rules_dic[country]['kwargs']

            self._add_local_eployment_data(
                    country,
                    data_file,
                    groupby,
                    on_var,
                    index_cols,
                    **output_kwargs_data)
            self._downscale_tab(
                    country,
                    data_file,
                    self.rules_dic[country]['name'],
                    **output_kwargs)
            self.country_data[country]['Z'] = self.IOtab.tables[country].df

            # Downscale Y
            Y = pd.DataFrame({
                'Y': self.mrio.Y.loc[country, country].sum(axis=1)
                # .mul(self.rules_dic[country]['pop_share'])
                })
            inx = [i in self.agg_data.loc[:, 'SECTOR CODE'] for i in Y.index]
            Y = Y.loc[inx, :]
            Y.loc[:, 'sector_code'] = [
                    self.agg_data.loc[i, sector_code] for i in Y.index]
            Y = Y.groupby('sector_code').sum()

            this_Z = self.IOtab.tables[country].df
            this_Z.index = Y.index
            this_Z.columns = Y.index
            self.country_data[country]['Z'] = this_Z
            self.country_data[country]['Y'] = Y

        # IMP
        inx = [i in self.agg_data.loc[:, 'SECTOR CODE'] for i in self.IMP.index.get_level_values(1)]
        self.IMP = self.IMP.loc[slice(None), inx]
        level = [
                self.agg_data.loc[i, sector_code]
                for i in self.IMP.index.get_level_values(1)]
        self.IMP = pd.DataFrame(self.IMP, columns=['imp'])
        self.IMP.loc[:, 'level'] = level
        self.IMP = self.IMP.join(self.global_IMP.fillna(0))
        self.IMP.loc[:, 'imp_new'] = self.IMP.loc[
                :, 'global'].sub(self.IMP.loc[:, 'imp']) + 1

        # GHG # TODO: validate
        self.GHG = self.GHG.div(self.mrio.A.loc['IDN', 'IDN'].sum(axis=0))
        self.GHG = 1 - self.GHG.div(self.GHG.max())

        inx = [i in self.agg_data.loc[:, 'SECTOR CODE'] for i in self.GHG.index.get_level_values(1)]
        self.GHG = self.GHG.loc[slice(None), inx]
        level = [
            self.agg_data.loc[i, sector_code]
            for i in self.GHG.index.get_level_values(1)]
        self.GHG = pd.DataFrame(self.GHG, columns=['ghg'])
        self.GHG.loc[:, 'level'] = level

    def _add_local_eployment_data(
            self, country, data_file, groupby, on_var, index_cols,
            **kwargs):
        """
        Load Employment data

        country = 'BR'
        data_file = 'data/recife_employmentdata.csv',
        groupby = 'nlevel1'
        on_var = 'city_job'
        index_cols = ['nlevel1', 'nlevel3',
                      'description_english', 'ntypequalifier']
        kwargs = {
            'usecols': [1, 2, 3, 4, 6],
            'header': 0,
            'names': [
                'city_job', 'nlevel3', 'description_english',
                'ntypequalifier', 'nlevel1']}
        """
        if 'header' in kwargs.keys():
            kwargs['header'] = int(kwargs['header'])
        data = pd.read_csv(data_file, **kwargs)

        data = pd.merge(
            data,
            data.groupby(groupby).sum().loc[:, on_var].reset_index(),
            on=groupby, how='left', suffixes=('', '_r'))

        new_var = on_var + '_r'
        data.loc[:, on_var] = data.loc[:, on_var].astype(float)
        data.loc[:, new_var] = data.loc[:, new_var].astype(float)
        data.loc[:, 'ppn_sect'] = data.loc[:, on_var].div(data.loc[:, new_var])

        # Generate standard and expanded MultiIndex
        midx = pd.MultiIndex.from_frame(data.loc[:, index_cols])

        self.country_data[country]['local_employment'] = data
        self.country_data[country]['local_employment_idx'] = midx

    def _get_multipiers(self, groupby='level'):
        # Compute multipliers
        print("compute multipliers")
        for country in self.countries:
            m = self.IMP.loc[country].fillna(0).groupby(groupby).mean()
            g = self.GHG.loc[country].fillna(0).groupby(groupby).mean()
            Y = self.country_data[country]['Y']
            data = self.country_data[country]['local_employment']
            midx = self.country_data[country]['local_employment_idx']
            Z, Y, [m, g] = IOT_expansion(
                    data, midx, self.IOtab.tables[country].df, Y, m=[m, g])
            outpmul, inpmul, mulmat = CE_multipliers(Z, midx)
            self.country_data[country]['midx'] = midx
            self.country_data[country]['mulmat'] = mulmat
            self.country_data[country]['inpmul'] = inpmul
            self.country_data[country]['outpmul'] = outpmul
            self.country_data[country]['Y'] = Y
            self.country_data[country]['Z'] = Z
            self.country_data[country]['m'] = m
            self.country_data[country]['g'] = g

    def compute_CE(self, use_mat=False, use_global=False, use_ghg=False, cap=1,
                   output_folder='output', file_name=False,
                   **kwargs):
        """
        Create CE Matrix
        """
        if 'mumat' not in self.country_data[self.countries[0]].keys():
            self._get_multipiers(**kwargs)
        for country in self.countries:
            data = self.country_data[country]['local_employment']
            mulmat = self.country_data[country]['mulmat']
            inpmul = self.country_data[country]['inpmul']
            outpmul = self.country_data[country]['outpmul']
            midx = self.country_data[country]['midx']
            Z = self.country_data[country]['Z']
            Y = self.country_data[country]['Y']
            m = self.country_data[country]['m']
            g = self.country_data[country]['g']
            if not file_name:
                file_name = country
            else:
                file_name = "{}_{}".format(country, file_name)
            mat = False
            ghg = False
            file_name_ext = ''
            if use_mat or use_global:
                if use_global:
                    mat = m.reset_index().loc[:, 'imp_new']
                    file_name_ext += '_mat_glo'
                else:
                    mat = m.reset_index().loc[:, 'imp']
                    mat[mat > cap] = cap
                    mat = 1 - mat
                    file_name_ext += '_mat'
            if use_ghg:
                ghg = g
                file_name_ext += '_ghg'

            file_name = '{}/{}_result{}.xlsx'.format(output_folder, file_name, file_name_ext)

            outtable = IOA(data, midx, Z, Y, mulmat, inpmul, outpmul, m=mat, g=ghg)
            self.country_data[country]['outtable'] = outtable
            outtable.to_excel(file_name)
