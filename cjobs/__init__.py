import datetime

__version__ = "1.2.36"

print('Date:', datetime.datetime.now().strftime('%A %b %d. %Y'))
print('Using cjobs version:', __version__)
